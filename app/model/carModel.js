
// Khái báo thư viện mongooseJS
const mongoose = require("mongoose");
// khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const carSchema = new Schema({
    model: {
        type: String,
        required: true
    },
    vId: {
        type: String,
        required: true,
        unique: true
    }
}, {
    timestamps: true
});
module.exports = mongoose.model("Car", carSchema);