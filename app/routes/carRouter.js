// khai báo thư viện express
const express = require('express');

// Import các middleware
const {
    getAllCarMiddleware,
    getCarMiddleware,
    postCarMiddleware,
    putCarMiddleware,
    deleteCarMiddleware
} = require('../middleware/carMiddleware');

// Tạo ra Router
const carRouter = express.Router();

// Import carController
const carController = require('../controllers/carController');

// Create new car
carRouter.post('/cars', postCarMiddleware, carController.createCar);

// Get all car
carRouter.get('/cars', getAllCarMiddleware, carController.getAllCar);

// Get a car by id
carRouter.get('/cars/:carId', getCarMiddleware, carController.getCarById);

// Update a car by id
carRouter.put('/cars/:carId', putCarMiddleware, carController.updateCarById);

// Delete a car by id
carRouter.delete('/cars/:carId', deleteCarMiddleware, carController.deleteCarById);

module.exports = { carRouter }