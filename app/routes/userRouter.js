// khai báo thư viện express
const express = require('express');

// Import các middleware
const {
    getAllUserMiddleware,
    getUserMiddleware,
    postUserMiddleware,
    putUserMiddleware,
    deleteUserMiddleware
} = require('../middleware/userMiddleware');

// Tạo ra Router
const userRouter = express.Router();

// Import userController
const userController = require('../controllers/userController');

// Create new user
userRouter.post('/users', postUserMiddleware, userController.createUser);

// Get all user
userRouter.get('/users', getAllUserMiddleware, userController.getAllUser);

// Get a user by id
userRouter.get('/users/:userId', getUserMiddleware, userController.getUserById);

// Update a user by id
userRouter.put('/users/:userId', putUserMiddleware, userController.updateUserById);

// Delete a user by id
userRouter.delete('/users/:userId', deleteUserMiddleware, userController.deleteUserById);

module.exports = { userRouter }