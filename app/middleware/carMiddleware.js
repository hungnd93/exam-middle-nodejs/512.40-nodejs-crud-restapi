const getAllCarMiddleware = (req, res, next) => {
    console.log("Get All Car!");
    next();
};

const getCarMiddleware = (req, res, next) => {
    console.log("Get a Car!");
    next();
};

const postCarMiddleware = (req, res, next) => {
    console.log("Create a Car!");
    next();
};

const putCarMiddleware = (req, res, next) => {
    console.log("Update a Car!");
    next();
};

const deleteCarMiddleware = (req, res, next) => {
    console.log("Delete a Car!");
    next();
};
module.exports  = {
    getAllCarMiddleware,
    getCarMiddleware,
    postCarMiddleware,
    putCarMiddleware,
    deleteCarMiddleware
}
