const getAllUserMiddleware = (req, res, next) => {
    console.log("Get All User!");
    next();
};

const getUserMiddleware = (req, res, next) => {
    console.log("Get a User!");
    next();
};

const postUserMiddleware = (req, res, next) => {
    console.log("Create a User!");
    next();
};

const putUserMiddleware = (req, res, next) => {
    console.log("Update a User!");
    next();
};

const deleteUserMiddleware = (req, res, next) => {
    console.log("Delete a User!");
    next();
};
module.exports  = {
    getAllUserMiddleware,
    getUserMiddleware,
    postUserMiddleware,
    putUserMiddleware,
    deleteUserMiddleware
}
